https://forums.xilinx.com/t5/Configuration/Centos-7-JTAG-Cable-Installation-Guide/td-p/656341

Requires Root Access to Install

Step 1)
Install Vivado

Step 2)
Unplug JTAG brick

Step 3)
Install libusb and fxload "sudo yum install fxload libusb"

If yum doesn't have fxload, run the following command "rpm -Uvh fxload-2002_04_1
1-16.el7.x86_64.rpm" in the console. If this doesn't work, find a compatible ver
sion of fxload on the internet.

Step 4)
Go to Cable Drivers Directory and Run Install Script. 
For me it was "$INSTALLATION_PATH/Xilinx/$TOOL/$VERSION/data/xicom/cable_drivers
/lin64/install_script/install_drivers".
$INSTALLATION_PATH = Where you installed Vivado or Hardware Server
$TOOL = Vivado or HWSRVR
$VERSION = Version of tool, i.e. 2014.4, 2015.1, 2015.2
You need to find the "setup_pcusb" script.

Run "sudo ./setup_pcusb"

Step 5)
Copy the file "xusbdfwu.rules" in this directory into the "/etc/udev/rules.d" folder.

Step 6)
Copy the files below in this directory into "/usr/share" directory.

xusbdfwu.hex
xusb_emb.hex
xusb_xlp.hex
xusb_xp2.hex
xusb_xpr.hex
xusb_xse.hex
xusb_xup.hex

Step 7)
Hook up JTAG cable. 
Orange light meets that everything installed correctly, and no FPGA is connected. 
Green light means everyting install correctly and FPGA is connected.
