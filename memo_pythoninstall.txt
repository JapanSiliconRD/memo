#!/bin/bash


#if [ ! -f "/opt/rh/devtoolset-2/enable" ]; then source memo_devtoolset_SLC6.txt ; fi
#source /opt/rh/devtoolset-2/enable

mkdir -p /usr/local/Python/tar_src
#for ipyv in 2.7.9 3.5.1
for ipyv in 3.7.1
do
    if [ -d /usr/local/Python/$ipyv ]; 
    then
	continue
    fi
    cd /usr/local/Python/tar_src
    curl -O https://www.python.org/ftp/python/$ipyv/Python-$ipyv.tgz
    cd ../
    tar -zxvf tar_src/Python-$ipyv.tgz 
    cd Python-$ipyv
    ./configure --prefix=/usr/local/Python/$ipyv --enable-shared
    make ; make install
    cd /usr/local/Python/$ipyv/bin
    cat > thispython.sh <<EOF
#! /bin/bash
source /opt/rh/devtoolset-2/enable
export PATH=/usr/local/Python/$ipyv/bin:${PATH}
export LD_LIBRARY_PATH=/usr/local/Python/$ipyv/lib:${LD_LIBRARY_PATH}
EOF
done

